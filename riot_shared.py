import bpy

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# PROPERTIES

# ENTITY TYPES
entityTypes = [\
    ("mdl-static" ,"mdl-static", "Static Model"),\
    ("empty" ,"empty", "Empty"),\
    ("msh-area" ,"msh-area", "Area Mesh"),\
    ("msh-nav" ,"msh-nav", "Navigational Mesh"),\
    ("decal" ,"decal", "Decal")\
]

# SCENE
bpy.types.Scene.riotAssetFolder = bpy.props.StringProperty(subtype='DIR_PATH')
bpy.types.Scene.riotMshEdit = bpy.props.StringProperty()

bpy.types.Scene.riotSunDirection = bpy.props.FloatVectorProperty(\
    name = "Sun Direction",\
    description = "Where the sun rays come from",\
    default = ( 0, 0, 1 ),\
    size = 3,\
    subtype = 'DIRECTION'\
    )
bpy.types.Scene.riotSunColor = bpy.props.FloatVectorProperty(\
    name = "Sun Color",\
    description = "The color of the direct sunlight",\
    default = ( 1, 1, 1 ),\
    max = 1,\
    min = 0,\
    size = 3,\
    subtype = 'COLOR'\
    )
bpy.types.Scene.riotSunIntensity = bpy.props.FloatProperty(\
    name = "Sun Intensity",\
    description = "Strength of the sun",\
    default = 1,\
    soft_max = 1,\
    min = 0,\
    subtype = 'FACTOR'\
    )
bpy.types.Scene.riotAmbientColor = bpy.props.FloatVectorProperty(\
    name = "Ambient Color",\
    description = "Color of the light from the sky",\
    default = ( 1, 1, 1 ),\
    max = 1,\
    min = 0,\
    size = 3,\
    subtype = 'COLOR'\
    )
bpy.types.Scene.riotAmbientIntensity = bpy.props.FloatProperty(\
    name = "Ambient Intensity",\
    description = "Brightness of the light from the sky",\
    default = 0.5,\
    soft_max = 1,\
    min = 0,\
    subtype = 'FACTOR'\
    )

# UPDATE FUNCS

def dupliGroupOff(self, context):
    ob = context.active_object
    
    if not ob.riotType:
        return None
    elif ob.riotType == 'empty':
        ob.dupli_type = 'NONE'
    else:
        ob.dupli_type = 'GROUP'
        
    return None

# OBJECT
bpy.types.Object.riotName = bpy.props.StringProperty(\
    name = "Name",\
    description = "Identifier used in level script"\
    )
bpy.types.Object.riotType = bpy.props.EnumProperty(\
    items = entityTypes,\
    name = "Entity Type",\
    description = "The function of this object",\
    update = dupliGroupOff\
    )
bpy.types.Object.riotMaterialNames = bpy.props.StringProperty(\
    name = "Materials",\
    description = "All materials available for this object"
    )
bpy.types.Object.riotSelectedMaterial = bpy.props.IntProperty(\
    name = "Material ID",\
    description = "ID of material to use",\
    default = 0,\
    min = 0
    )
bpy.types.Object.riotTimestamp = bpy.props.IntProperty(\
    name = "Time Stamp",\
    description = "Time of last object update"\
    )
bpy.types.Object.riotDecalPath = bpy.props.StringProperty(\
    name = "Material",\
    description = "Path to material file used by decal"\
    )

bpy.types.Object.riotInitFunc = bpy.props.StringProperty(\
    name = "Init", description = "Called when scenario is started")
bpy.types.Object.riotTickFunc = bpy.props.StringProperty(\
    name = "Tick", description = "Called several times a second, but not every update")
bpy.types.Object.riotUpdateFunc = bpy.props.StringProperty(\
    name = "Update", description = "Called every update")
bpy.types.Object.riotDestroyFunc = bpy.props.StringProperty(\
    name = "Destroy", description = "Called before object is removed")

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# GLOBAL VARIABLES

levelSceneName = "level"
assetSceneName = "asset"
mshNavSceneName = "msh-nav"
mshAreaSceneName = "msh-area"
otherSceneName = "other"

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# GLOBAL FUNCTIONS

# # # # # # # # # # # # # # # # # # # #
# GET SCENE FUNCTION
# Returns the scene called what inputName is, if that scene does not exist,
# create that scene.
def getScene(inputName):
    for scene in bpy.data.scenes:
        if(scene.name == inputName):
            return scene

    return bpy.data.scenes.new(inputName)