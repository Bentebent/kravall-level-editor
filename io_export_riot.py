bl_info = {
 "name": "Kravall Level Export (.lua)",
 "author": "Lukas Orsvärn",
 "version": (1, 2, 11),
 "blender": (2, 69, 0),
 "location": "File > Export > Kravall Level (.lua)",
 "description": "Export as Kravall Level (.lua)",
 "wiki_url": "",
 "tracker_url": "",
 "category": "Import-Export"}
 
import bpy, math, mathutils, imp, os.path, os

import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)
import riot_shared as sha

# ExportHelper is a helper class, defines filename and
# invoke() function which calls the file selector.
from bpy_extras.io_utils import ExportHelper
from bpy.props import StringProperty, BoolProperty, EnumProperty
from bpy.types import Operator

write = True

tabSize = "    "

# Nav Mesh:
numNodes = 0
nodeVertPos = []
nodeDestinationList = []

'''
nodeVertPos looks like this:
LIST[ NODE[ POS(X, Y), POS(X, Y), POS(X, Y), POS(X, Y) ], NODE... ]

nodeDestinationList looks like this:
LIST[ NODE[X, Y, Z, W], NODE... ]
'''

areaMeshList = []
mdlStaticList = []

class sunSettings():
    direction = ( 0, 0, 0 )
    color = ( 0, 0, 0 )
    intensity = 0
sun = sunSettings()

class ambientSettings():
    color = ( 0, 0, 0 )
    intensity = 0
ambient = ambientSettings()

class mdlStatic():
    name = ""
    co = (0, 0, 0)
    rot = (0, 0, 0, 0)
    mdl = ""
    mat = ""
    initFunc = ""
    updateFunc = ""
    tickFunc = ""
    destroyFunc = ""

rotX90 = mathutils.Matrix((\
    (1.0, 0.0, 0.0, 0.0),\
    (0.0, 0.0, -1.0, 0.0),\
    (0.0, 1.0, 0.0, 0.0),\
    (0.0, 0.0, 0.0, 1.0)\
))
rotX90neg = mathutils.Matrix((\
    (1.0, 0.0, 0.0, 0.0),\
    (0.0, 0.0, 1.0, 0.0),\
    (0.0, -1.0, 0.0, 0.0),\
    (0.0, 0.0, 0.0, 1.0)\
))

class ExportRiotLevel(Operator, ExportHelper):
    """Export the scene as a Kravall level"""
    bl_idname = "export.riot_level"
    bl_label = "Export Kravall Level"
    
    # ExportHelper mixin class uses this
    filename_ext = ".lua"

    filter_glob = StringProperty(
            default="*.lua",
            options={'HIDDEN'},
            )

    # List of operator properties, the attributes will be assigned
    # to the class instance from the operator settings before calling.
    export_nav = BoolProperty(
            name="msh-nav",
            description="Export the navigation surfaces",
            default=True,
            )
    export_area = BoolProperty(
            name="msh-area",
            description="Export the area entities",
            default=True,
            )
    export_mdl_static = BoolProperty(
            name="mdl-static",
            description="Export static models",
            default=True,
            )
    export_empty = BoolProperty(
            name="Empty",
            description="Export empties",
            default=True,
            )
    export_camera = BoolProperty(
            name="Camera",
            description="Export cameras",
            default=True,
            )
    export_light = BoolProperty(
            name="Light",
            description="Export all kinds of light",
            default=True,
            )
    export_decal = BoolProperty(
            name="Decal",
            description="Export decals",
            default=True,
            )
    '''
    type = EnumProperty(
            name="Example Enum",
            description="Choose between two items",
            items=(('OPT_A', "First Option", "Description one"),
                   ('OPT_B', "Second Option", "Description two")),
            default='OPT_A',
            )
    '''
    def execute(self, context):
        return self.start(context, self.filepath, self.export_nav,\
            self.export_area, self.export_mdl_static, self.export_empty,\
            self.export_camera, self.export_light, self.export_decal)

    def start(\
        self, context, filepath, export_nav, export_area, export_mdl_static,\
        export_empty, export_camera, export_light, export_decal):

        global write, numNodes, nodeVertPos
        filepath = filepath.replace("\\", "/")

        numNodes = 0
        nodeVertPos = []
        writeNav = False

        # EXPORT NAV
        if export_nav:
            if self.navMeshCount() > 1:
                self.report(\
                    {'ERROR'},\
                    'Found {} nav meshes instead of 1, see console for details'\
                    .format(self.navMeshCount()))

                self.printNavMeshes()
                return{'FINISHED'}

            navEmpty = None
            for ob in sha.getScene(sha.levelSceneName).objects:
                if ob.type == 'EMPTY' and ob.riotType == 'msh-nav':
                    navEmpty = ob
                    break
            if(navEmpty == None):
                self.report({'WARNING'}, 'No nav mesh found')
            else:
                writeNav = True
                self.exportNavMesh(context, navEmpty, filepath)
                if write:
                    self.writeNavFile(filepath)
        else:
            writeNav = False
        
        # CREATE SCENARIO FILE
        if not os.path.exists( filepath.rpartition('/')[0] ):
            os.makedirs( filepath.rpartition('/')[0] )
        f = open(filepath, 'w', encoding='utf-8')
        self.clearFile(f)
        self.writeHeader(f, filepath, writeNav, export_camera)

        self.exportGlobalLight( export_light )
        self.writeGlobalLight(f)

        if export_area:
            status = self.exportMshArea(context)
            if status:
                self.report(\
                {'WARNING'},\
                "During msh-area export: {}".format(status))
            if write:
                self.writeMshArea(f)

        if export_light:
            self.exportLightEntities(f)

        if export_mdl_static:
            self.exportMdlStatic(context)
            self.writeMdlStatic(f)

        if export_empty:
            self.writeEmpties(f)

        if export_decal:
            self.writeDecals(f)

        self.writeFooter(f)
        f.close()

        # CREATE SCENARIO SCRIPT FILE
        self.createScenarioScriptFile(filepath)

        return{'FINISHED'}

    def getDupliGroupObject(self, empty):
        if empty.type == 'EMPTY':
            if empty.dupli_group:
                return empty.dupli_group.objects[0]

        return None

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# NAV MESH FILE

    def exportNavMesh(self, context, empty, filepath):
        global numNodes, nodeVertPos, nodeDestinationList, write
        nodeDestinationList = []
        nodeVertPos = []
        numNodes = []

        numNodes = 0
        scene = context.scene

        if empty.dupli_group is None:
            self.report( {'ERROR'}, \
                'No nav mesh found in the nav mesh dupli group' )
            write = False
            return

        loc = empty.location.xy
        emptyMatrix = empty.matrix_basis

        navMesh = empty.dupli_group.objects[0].to_mesh(\
            scene, False, 'PREVIEW', False, False)

        import bmesh
        bm = bmesh.new()
        bm.from_mesh(navMesh)
            
        for face in bm.faces:
            bmesh.utils.face_flip(face)

        # Make sure there are no ngons.
        for face in bm.faces:
            if len(face.verts) > 4:
                co = face.calc_center_median().to_tuple()
                self.report({'ERROR'},\
    'Nav mesh contains ngon in face at {:.4f}, {:.4f}, {:.4f} in local space'\
                    .format(co[0], co[1], co[2]))
                write = False
                return

        for faceA in bm.faces:
            # Get vertex positions.
            numNodes += 1
            faceVertPos = []

            for vertexA in faceA.verts:
                vertexMat = mathutils.Matrix.Translation(vertexA.co)
                vertexPos = ( empty.matrix_basis * vertexMat ).translation
                faceVertPos.append( vertexPos.xy.to_tuple() )

            # Get line to node associations.
            destination = []
            currVert = int
            nextVert = int
            for vertexA in range(-len(faceA.verts), 0):
                currVert = faceA.verts[vertexA].index
                nextVert = faceA.verts[vertexA + 1].index

                otherFaceIndex = -1
                for faceB in bm.faces:
                    
                    if faceA is not faceB:
                        indexList = []
                        for vertexB in faceB.verts:
                            indexList.append(vertexB.index)
                        if (currVert in indexList) and (nextVert in indexList):
                            otherFaceIndex = faceB.index
                
                destination.append(otherFaceIndex)

            # If it's a triangle, add a -2 to the end of the destination list
            # and duplicate the first value at the end.
            if len(destination) == 3:
                destination.append(-2)
                faceVertPos.append(faceVertPos[0])

            # Append values to their master lists
            nodeVertPos.append(faceVertPos)
            nodeDestinationList.append(destination)
        
        bm.free()

        pass

    def writeNavPath(self, inputFile, filepath):
        global tabSize
        ts = tabSize

        f = inputFile

        navFileName = ""
        folder = filepath.rpartition("/riot/")[2].rpartition("/")[0]
        
        navFileName = filepath.split("/")[-1]
        part = navFileName.rpartition(".")
        navFileName = part[0] + part[1] + "nav"

        fullPath = folder + "/" + navFileName

        f.write(ts + 'scen.asm:specific_content( core.contentmanager.load( ')
        f.write('core.loaders.NavigationMeshLoader, "{}", '\
            .format(fullPath))
        f.write('function( value ) end, false ) )')
        f.write('\n\n')

    def writeNavFile(self, filepath):
        global nodeDestinationList

        ndl = nodeDestinationList
        nvp = nodeVertPos

        part = filepath.rpartition(".")
        navFilepath = part[0] + part[1] + "nav"
        
        if not os.path.exists( navFilepath.rpartition('/')[0] ):
            os.makedirs( navFilepath.rpartition('/')[0] )
        f = open(navFilepath, 'w', encoding='utf-8')
        f.truncate()

        f.write("{}\n\n".format(numNodes))
        for node in range(len(ndl)):
            for pair in nvp[node]:
                f.write("{} {} ".format(pair[0], -pair[1]))

            f.write("\n{} {} {} {}\n\n"\
                .format(ndl[node][0], ndl[node][1], ndl[node][2], ndl[node][3]))

        f.close()

    def navMeshCount(self):
        count = 0

        for ob in sha.getScene(sha.levelSceneName).objects:
            if ob.type == 'EMPTY' and ob.riotType == 'msh-nav':
                count += 1

        return count

    
    def printNavMeshes(self):
        print("Found more than one nav mesh!")
        print("Nav mesh object names:")
        for ob in bpy.data.objects:
            if ob.type == 'EMPTY' and ob.riotType == 'msh-nav':
                print(ob.name)

        return

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# SCENARIO FILE

    def exportMshArea(self, context):
        global areaMeshList
        areaMeshList = []
        lvlScn = sha.getScene(sha.levelSceneName)


        for ob in lvlScn.objects:
            if ob.type == 'EMPTY' and ob.riotType == 'msh-area':
                msh = self.getDupliGroupObject(ob)
                
                
                if msh == None:
                    return "Could not find a mesh in {}".format(ob.name)
                
                import bmesh
                bm = bmesh.new()
                bm.from_object(msh, context.scene)

                if not len(bm.faces):
                    return "Could not find any polygons in {}".format(ob.name)
                if len(bm.faces) > 1:
                    return "There is more than one polygon in {}"\
                    .format(ob.name)
                if len(bm.faces[0].verts) < 4:
                    return "There are less than 4 vertices " + \
                        "in the polygon in {}"\
                    .format(ob.name)
                if len(bm.faces[0].verts) > 4:
                    return "There are more than 4 vertices in the polygon in {}\
                    ".format(ob.name)

                bmesh.utils.face_flip(bm.faces[0])
                name = ob.riotName
                loc = ob.location.xy.to_tuple()
                verts = bm.faces[0].verts
                vertList = []
                vl = vertList

                for vertex in verts:
                    noTransBasisMat = ob.matrix_basis.copy()
                    noTransBasisMat[3][3] = 0
                    noTransBasisMat[2][3] = 0
                    noTransBasisMat[1][3] = 0
                    noTransBasisMat[0][3] = 0

                    vertexMat = mathutils.Matrix.Translation(vertex.co)
                    vertexMat = noTransBasisMat * vertexMat

                    vertexPos = vertexMat.translation

                    vl.append( vertexPos.xy.to_tuple() )

                initFunc =\
                    "" if not len(ob.riotInitFunc) else ob.riotInitFunc
                updateFunc =\
                    "" if not len(ob.riotTickFunc) else ob.riotTickFunc

                areaMeshList.append(\
                    (name, loc, vl[0], vl[1], vl[2], vl[3], initFunc, updateFunc))
                
                bm.free()

        return None

    def writeScriptFilePath(self, inputFile, filepath):
        f = inputFile

        part = filepath.rpartition("/riot/")[2].rpartition(".")
        scriptFilePath = part[0] + "_script" + part[1] + part[2]

        f.write('local script = dofile "{}"( scen )\n\n'\
            .format(scriptFilePath))

    def clearFile(self, inputFile):
        inputFile.truncate()

    def writeHeader(self, inputFile, filepath, writeNav, writeCameras):
        global tabSize
        ts = tabSize
        f = inputFile

        
        f.write('local entities = require "entities"\n')
        f.write('local scenario = require "scenario"\n')
        f.write('local scen = scenario.new()\n')
        f.write('\n')
        f.write('scen.name = "Default Scenario Name"\n')
        f.write('scen.description = "Default Description"\n')
        f.write('\n')
        if writeCameras:
            self.writeCameras(f)
            f.write('\n')
        self.writeScriptFilePath(f, filepath)
        f.write('function scen:load()\n')
        f.write(ts + 'local ambient = entities.get "ambientLight"\n')
        f.write(ts + 'local directional = entities.get "directionalLight"\n')
        f.write(ts + 'local pointLight = entities.get "pointLight"\n')
        f.write(ts + 'local spotLight = entities.get "spotLight"\n')
        f.write(ts + 'local area = entities.get "area"\n')
        f.write(ts + 'local rioter = entities.get "rioter"\n')
        f.write(ts + 'local staticModel = entities.get "staticModel"\n')
        f.write(ts + 'local empty = entities.get "empty"\n')
        f.write(ts + 'local decal = entities.get "decal"\n')
        f.write(ts + 'local ent\n')
        f.write(ts + '\n')
        if writeNav:
            self.writeNavPath(f, filepath)
            f.write(ts + '\n')
        f.write(ts + 'local function genF( e, f )\n')
        f.write(ts + '    if type( script[f] ) == "function"  then\n')
        f.write(ts + '        return function( delta ) ' + \
            'script[ f ]( e, delta ) end\n')
        f.write(ts + '    else\n')
        f.write(ts + '        error( "Function " .. f .. " ' + \
            'does not exist in script file" )\n')
        f.write(ts + '    end\n')
        f.write(ts + 'end\n')
        f.write(ts + '\n')

    def writeFooter(self, inputFile):
        f = inputFile

        f.write('end\n')
        f.write('return scen\n')

    def writeMshArea(self, inputFile):
        global tabSize
        ts = tabSize
        f = inputFile


        for item in areaMeshList:
            f.write(ts + "ent = area( scen,")
            f.write(" {{{}, 0, {}}},".format(item[1][0], -item[1][1]))
            f.write(" {{{}, {},".format(item[2][0], -item[2][1]))
            f.write(" {}, {},".format(item[3][0], -item[3][1]))
            f.write(" {}, {},".format(item[4][0], -item[4][1]))
            f.write(" {}, {}}},".format(item[5][0], -item[5][1]))
            f.write(" \"{}\" )\n".format(item[0]))
            self.writeFunction(f, item[6], 'INIT')
            self.writeFunction(f, item[7], 'TICK')
            f.write("\n")

# # # # # # # # # # # # # # # # # # # #
# MDL-STATIC EXPORT

    def exportMdlStatic(self, context):
        global mdlStaticList, rotX90, rotX90neg

        mdlStaticList = []
        lvlScn = sha.getScene(sha.levelSceneName)

        for ob in lvlScn.objects:
            if ob.type != 'EMPTY'\
            or ob.riotType != 'mdl-static'\
            or ob.dupli_group == None:
                continue

            mshOb = ob.dupli_group.objects[0]
            newItem = mdlStatic()
            obMatrix = rotX90neg * ob.matrix_local * rotX90

            newItem.name = ob.riotName
            newItem.co = ob.location.to_tuple()
            rq = obMatrix.to_quaternion()
            newItem.rot = (rq[0], rq[1], rq[2], rq[3])
            newItem.scale = ( obMatrix.to_scale().x, obMatrix.to_scale().y, obMatrix.to_scale().z )

            # mdl
            objectPath = bpy.path.abspath(\
                lvlScn.riotAssetFolder + \
                mshOb.name).replace("\\", "/")
            f = open(objectPath, 'r', encoding='utf-8')
            entireFile = f.read()
            f.close()
            mdlPath = entireFile.partition('"')[2].partition('"')[0]
            newItem.mdl = mdlPath
            newItem.mat = \
                mshOb.riotMaterialNames.split(",")[ ob.riotSelectedMaterial ]
            newItem.initFunc = ob.riotInitFunc
            newItem.updateFunc = ob.riotUpdateFunc
            newItem.tickFunc = ob.riotTickFunc
            newItem.destroyFunc = ob.riotDestroyFunc

            mdlStaticList.append(newItem)

        return

    def writeMdlStatic(self, inputFile):
        global mdlStaticList, tabSize
        ts = tabSize

        f = inputFile

        for item in mdlStaticList:
            f.write(ts + "ent = staticModel( scen, " + \
                "{}, {}, {}, {}, {}, {}, {}, {{ {}, {}, {} }}, \"{}\", \"{}\", \"{}\" )\n"\
                .format(\
                    item.co[0], item.co[2], -item.co[1],\
                    item.rot[1], item.rot[2], item.rot[3], item.rot[0],\
                    item.scale[0], item.scale[1], item.scale[2],\
                    item.mdl,\
                    item.mat,\
                    item.name\
                )
            )
            self.writeFunction(f, item.initFunc, 'INIT')
            self.writeFunction(f, item.updateFunc, 'UPDATE')
            self.writeFunction(f, item.tickFunc, 'TICK')
            self.writeFunction(f, item.destroyFunc, 'DESTROY')

        f.write("\n")

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# GLOBAL LIGHT EXPORT

    def exportGlobalLight(self, export_light):
        global sun, ambient

        if export_light:
            lvlScn = sha.getScene( sha.levelSceneName )

            # SUN
            x, y, z = lvlScn.riotSunDirection
            sun.direction = ( x, y, z )

            sr, sg, sb = lvlScn.riotSunColor
            sun.color = ( sr, sg, sb )
            
            sun.intensity = lvlScn.riotSunIntensity

            # AMBIENT
            ar, ag, ab = lvlScn.riotAmbientColor
            ambient.color = ( ar, ag ,ab )

            ambient.intensity = lvlScn.riotAmbientIntensity
        else:
            sun.direction = ( .666, .333, .666 )
            sun.color = ( 1, 1, 1 )
            sun.intensity = .5

            ambient.color = ( 1, 1, 1 )
            ambient.intensity = .5

        return None


    def writeGlobalLight(self, inputFile):
        global sun, ambient, tabSize
        ts = tabSize
        amb = ambient

        f = inputFile

        f.write(ts + 'directional( scen, {}, {}, {}, {}, {}, {}, {} )\n'\
            .format(
                -sun.direction[0], -sun.direction[2], sun.direction[1],\
                sun.color[0], sun.color[1], sun.color[2],\
                sun.intensity
            )
        )
        f.write(ts + 'ambient( scen, {}, {}, {}, {} )\n'\
            .format(
                amb.color[0], amb.color[1], amb.color[2],\
                amb.intensity
            )
        )
        f.write("\n")

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# LIGHT ENTITY EXPORT

    def exportLightEntities(self, inputFile):
        global tabSize
        ts = tabSize
        f = inputFile
        lvlScn = sha.getScene(sha.levelSceneName)
        
        frontAxis = mathutils.Vector()
        frontAxis.z = -1

        for ob in lvlScn.objects:
            if ob.type != 'LAMP':
                continue

            if ob.data.type == 'POINT':
                f.write(ts + "ent = pointLight( " + \
                    "scen, {}, {}, {}, {}, {}, {}, {}, {}, \"{}\" )\n"\
                    .format(\
                        ob.location.x, ob.location.z, -ob.location.y,\
                        ob.data.color.r, ob.data.color.g, ob.data.color.b,\
                        ob.data.distance,\
                        ob.data.energy,\
                        ob.riotName\
                    )
                )
                self.writeCbFunctions(f, ob)
            if ob.data.type == 'SPOT':
                frontVec = ob.rotation_euler.to_matrix() * frontAxis

                f.write(ts + "ent = spotLight( " + \
        "scen, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, \"{}\" )\n"\
                    .format(\
                        ob.location.x, ob.location.z, -ob.location.y,\
                        frontVec.x, frontVec.z, -frontVec.y,\
                        ob.data.color.r, ob.data.color.g, ob.data.color.b,\
                        ob.data.spot_size / 2,\
                        ( ob.data.spot_size * ob.data.spot_blend ) / 2,\
                        ob.data.distance,\
                        ob.data.energy,\
                        ob.riotName\
                    )
                )
                self.writeCbFunctions(f, ob)
        f.write("\n")

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# CAMERA EXPORT
    def writeCameras(self, inputFile):
        global tabSize, rotX90, rotX90neg
        ts = tabSize
        f = inputFile
        lvlScn = sha.getScene(sha.levelSceneName)

        f.write( "scen.cameras = scen.cameras or {}\n" )
        for ob in lvlScn.objects:
            if ob.type == 'CAMERA' and ob.riotName != "":
                viewMatrix = ( rotX90neg * ob.matrix_basis ).inverted()
                tq = viewMatrix.to_quaternion()
                quaternion = mathutils.Vector(( tq.x, tq.y, tq.z, tq.w ))

                f.write( 'scen.cameras["{}"] = {{\n'.format(ob.riotName)+\
                    ts + 'view = core.glm.mat4.new(\n'+\
                    ts + '{}, {}, {}, {},\n'.format( *viewMatrix.col[0] )+\
                    ts + '{}, {}, {}, {},\n'.format( *viewMatrix.col[1] )+\
                    ts + '{}, {}, {}, {},\n'.format( *viewMatrix.col[2] )+\
                    ts + '{}, {}, {}, {} ),\n'.format( *viewMatrix.col[3] )+\
                    ts + 'fov = {},\n'.format( ob.data.angle_x )+\
                    ts + 'quaternion = core.glm.quat.new( {}, {}, {}, {} ),\n'\
                        .format( *quaternion )+\
                    ts + 'translation = core.glm.vec3.new( {}, {}, {} )\n'\
                        .format( *viewMatrix.inverted().to_translation() )+\
                    '}\n'\
                )

        return

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# DECAL EXPORT
    def writeDecals(self, inputFile):
        global tabSize, rotX90, rotX90neg
        ts = tabSize
        lvlScn = sha.getScene(sha.levelSceneName)
        f = inputFile
        
        for ob in lvlScn.objects:
            if ob.type == 'EMPTY' and ob.riotType == "decal":
                fixedMatrix = (rotX90neg * ob.matrix_basis).inverted()
                mat = ob.riotDecalPath
                if len( mat ) == 0:
                    continue

                obMatrix = rotX90neg * ob.matrix_local * rotX90

                name = ob.riotName
                co = obMatrix.to_translation()
                rot = obMatrix.to_quaternion()
                rot = ( rot.x, rot.y, rot.z, rot.w )
                scale = obMatrix.to_scale()

                
                f.write(ts + 'ent = decal( scen, ' + \
                '{}, {}, {}, '\
                    .format( *co ) + \
                '{}, {}, {}, {}, '\
                    .format( *rot ) + \
                '{}, {}, {}, '\
                    .format( *scale ) + \
                '"assets/{}", '\
                    .format( mat ) + \
                '"{}" )\n'\
                    .format( name ) \
                )
                
                self.writeCbFunctions(f, ob)

        return

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# EMPTY EXPORT
    def writeEmpties(self, inputFile):
        global tabSize, rotX90, rotX90neg
        ts = tabSize
        f = inputFile
        lvlScn = sha.getScene(sha.levelSceneName)

        for ob in lvlScn.objects:
            if ob.type != 'EMPTY' \
            or ob.riotType != 'empty':
                continue

            obMatrix = rotX90neg * ob.matrix_basis * rotX90
            
            name = ob.riotName
            pos = obMatrix.translation
            tmpRot = obMatrix.to_quaternion()
            rot = ( tmpRot[1], tmpRot[2], tmpRot[3], tmpRot[0] )
            scale = obMatrix.to_scale()

            f.write(ts + 'ent = empty( scen, ' + \
                '"{}", '\
                    .format( name ) + \
                '{}, {}, {}, '\
                    .format( *pos ) + \
                '{}, {}, {}, {}, '\
                    .format( *rot ) + \
                '{}, {}, {} )\n'\
                    .format( *scale )
            )
            self.writeCbFunctions(f, ob)
            f.write('\n')


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# CALLBACK EXPORT
    def writeCbFunctions(self, inputFile, ob):
        self.writeFunction(inputFile, ob.riotInitFunc, 'INIT')
        self.writeFunction(inputFile, ob.riotUpdateFunc, 'UPDATE')
        self.writeFunction(inputFile, ob.riotTickFunc, 'TICK')
        self.writeFunction(inputFile, ob.riotDestroyFunc, 'DESTROY')

    def writeFunction(self, inputFile, function, cbType):
        global tabSize
        ts = tabSize

        if function == "":
            return
        if inputFile == None:
            return

        cbTypeStr = ""
        if cbType == 'INIT':
            cbTypeStr = "Init"
        elif cbType == 'UPDATE':
            cbTypeStr = "Update"
        elif cbType == 'TICK':
            cbTypeStr = "Tick"
        elif cbType == 'DESTROY':
            cbTypeStr = "Destroy"
        else:
            return

        inputFile.write(ts + 'scen:register{}Callback( genF( ent, "{}" ) )\n'\
            .format( cbTypeStr, function ))

        return

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# SCRIPT FILE

    def createScenarioScriptFile(self, filepath):
        global tabSize
        ts = tabSize
        part = filepath.rpartition(".")
        filepath = part[0] + "_script" + part[1] + part[2]

        open(filepath, 'a', encoding='utf-8').close
        f = open(filepath, 'r', encoding='utf-8')
        entireFile = f.read()
        f.close()

        if not os.path.exists( filepath.rpartition('/')[0] ):
            os.makedirs( filepath.rpartition('/')[0] )
        if entireFile == "":
            f = open(filepath, 'w', encoding='utf-8')
            f.write(\
                'return function( scen )\n'+\
                ts + 'local T = {}\n'+\
                ts + '\n' + \
                ts + 'scen.gamemode = require "gamemodes/kravall":new()\n' +\
                ts + 'scen:registerInitCallback( ' + \
                     'function() scen.gamemode:init() end )\n' +\
                ts + 'scen:registerUpdateCallback( ' + \
                     'function(delta) scen.gamemode:update(delta) end )\n' +\
                ts + 'scen:registerDestroyCallback( ' + \
                     'function() scen.gamemode:destroy() end )\n' +\
                ts + '\n'+\
                ts + '-- Script goes here\n'+\
                ts + '\n'+\
                ts + 'return T\n'+\
                'end\n'
            )
            f.close()

        return



# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# OTHER

# Only needed if you want to add into a dynamic menu
def menu_func_export(self, context):
    self.layout.operator(\
        ExportRiotLevel.bl_idname, text="Kravall Level Export")


def register():
    bpy.utils.register_class(ExportRiotLevel)
    bpy.types.INFO_MT_file_export.append(menu_func_export)


def unregister():
    bpy.utils.unregister_class(ExportRiotLevel)
    bpy.types.INFO_MT_file_export.remove(menu_func_export)


if __name__ == "__main__":
    register()

    # test call
    #bpy.ops.export.riot_level('INVOKE_DEFAULT')