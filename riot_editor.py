bl_info = {
 "name": "Kravall Editor",
 "author": "Lukas Orsvärn",
 "version": (1, 2, 11),
 "blender": (2, 69, 0),
 "location": "Tool & property panels",
 "description": "Edit levels for Kravall",
 "wiki_url": "",
 "tracker_url": "",
 "category": "3D View"}

import bpy, os, mathutils, fnmatch, time

import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)
import riot_shared as sha

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# TO DO

# Break out msh-area and msh-nav from entity system.

# Ensure that the default startup had no crap in it and that all the relevant
# menus are open by default.

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# NOTES

# To create a custom list of stuff to search through, maybe put it in a custom
# property and manually update it with the items you want?

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# GLOBAL VARIABLES
mshNavColor = mathutils.Color((.3, .7, .8))
mshAreaColor = mathutils.Color((.3, .8, .3))

icon = { \
    'mdl-static' : 'MESH_CUBE', \
    'msh-area' : 'MESH_PLANE', \
    'msh-nav' : 'MESH_GRID', \
    'camera' : 'CAMERA_DATA', \
    'light' : 'LAMP', \
    'light-spot' : 'LAMP_SPOT', \
    'folder' : 'FILESEL', \
    'export' : 'EXPORT', \
    'empty' : 'OUTLINER_OB_EMPTY', \
    'light-point' : 'LAMP_POINT', \
    'decal' : 'TEXTURE' \
}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# PROPERTIES
bpy.types.Scene.riotExportPath = bpy.props.StringProperty(subtype='FILE_PATH')
bpy.types.Scene.exportNav = \
    bpy.props.BoolProperty(name = "Nav", default = True)
bpy.types.Scene.exportArea = \
    bpy.props.BoolProperty(name = "Area", default = True)
bpy.types.Scene.exportMdlStatic = \
    bpy.props.BoolProperty(name = "Mdl", default = True)
bpy.types.Scene.exportEmpty = \
    bpy.props.BoolProperty(name = "Empty", default = True)
bpy.types.Scene.exportCamera = \
    bpy.props.BoolProperty(name = "Cam", default = True)
bpy.types.Scene.exportLight = \
    bpy.props.BoolProperty(name = "Light", default = True)
bpy.types.Scene.exportDecal = \
    bpy.props.BoolProperty(name = "Decal", default = True)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# GLOBAL FUNCTIONS

# # # # # # # # # # # # # # # # # # # #
# SET COLOR
# Sets the material and color of an object to the input color.
def setColor(ob, color):
    if ob.type != 'MESH':
        return

    rightMat = None
    
    for mat in bpy.data.materials:
        if mat.diffuse_color == color:
            rightMat = mat
            break

    if not rightMat:
        rightMat = bpy.data.materials.new("Material")
        rightMat.diffuse_color = color

    ob.data.materials.clear()
    ob.data.materials.append(rightMat)

    return

# # # # # # # # # # # # # # # # # # # #
# SET SCENE COLOR
# Sets the material and color of all objects in a scene to the input color
def setSceneColor(scene, color):
    for ob in scene.objects:
        setColor(ob, color)

    return

# # # # # # # # # # # # # # # # # # # #
# SET UP SCENE COLORS
#
# Exits edit mode on a dupli group object.
def setUpSceneColors():
    global mshNavColor, mshAreaColor
    setSceneColor(sha.getScene(sha.mshNavSceneName), mshNavColor)
    setSceneColor(sha.getScene(sha.mshAreaSceneName), mshAreaColor)

    return

# # # # # # # # # # # # # # # # # # # #
# ENSURE SCENE LINKED FUNCTION
# Makes sure the asset is in the specified scene.
def ensureSceneLinked(inputAsset, inputScene, exclusive = False):
    if exclusive:
        # Unlink from all scenes but the inputScene.
        for scene in bpy.data.scenes:
            if scene.name != inputScene.name:
                if inputAsset in list(scene.objects):
                    scene.objects.unlink(inputAsset)

    for asset in inputScene.objects:
        if asset == inputAsset:
            return True

    inputScene.objects.link(inputAsset)
    return False

# # # # # # # # # # # # # # # # # # # #
# CREATE GROUPED MESH OBJECT
#
# If a mesh does not exist in the sent in emptys dupli group, it creates a mesh,
# links it to an object and a specified scene as well as links that group to the
# specified emptys dupligroup.
#
# If a mesh does exist, it does nothing.
def createGroupedMeshObject(context, empty, scene, name):
    curScn = context.scene
    ob = None

    if not context.active_object.type == 'EMPTY':
        return 'NO_EMPTY'

    if not empty.dupli_group:
        group = bpy.data.groups.new(name)
        empty.dupli_group = group
    elif len(empty.dupli_group.objects) > 1:
        return 'SEVERAL_OBJECTS'

    if len(empty.dupli_group.objects) < 1:
        group = empty.dupli_group
        mesh = bpy.data.meshes.new(name)
        ob = bpy.data.objects.new(name, mesh)
        
        curScn.objects.link(ob)
        curScn.objects.unlink(ob)
        group.objects.link(ob)
    else:
        ob = empty.dupli_group.objects[0]

    ensureSceneLinked(ob, scene, True)
    
    return 'FINISHED'

# # # # # # # # # # # # # # # # # # # #
# EDIT DUPLI GROUP OBJECT
#
# Enters edit mode on the first mesh in the dupli group of the input empty.
def editDupliGroupObject(context, empty):
    if not empty.dupli_group:
        return 'NO_GROUP'
    elif len(empty.dupli_group.objects) < 1:
        return 'NO_OBJECT'
    elif len(empty.dupli_group.objects) > 1:
        return 'SEVERAL_OBJECTS'

    scn = context.scene
    ob = empty.dupli_group.objects[0]

    editOb = bpy.data.objects.new(empty.name+"_edit", ob.data)
    scn.objects.link(editOb)

    editOb.location = empty.location

    scn.objects.active = editOb
    editOb.select = True

    empty.hide = True

    bpy.ops.object.mode_set(mode = 'EDIT')

    if len(editOb.data.polygons) < 1:
        bpy.ops.mesh.primitive_plane_add(location = empty.location)

    return 'FINISHED'

# # # # # # # # # # # # # # # # # # # #
# DONE EDIT DUPLI GROUP OBJECT
#
# Exits edit mode on a dupli group object.
def doneEditDupliGroupObject(context, empty):
    scn = context.scene
    bpy.ops.object.mode_set(mode = 'OBJECT')

    ob = context.active_object

    scn.objects.unlink(ob)
    empty.hide = False
    scn.objects.active = empty
    empty.select = True

    return 'FINISHED'

# # # # # # # # # # # # # # # # # # # #
# REFRESH ASSETS OPERATOR
class RefreshAssets(bpy.types.Operator):
    """Update changed and add new assets"""
    bl_idname = "riot.refresh_assets"
    bl_label = "Refresh"

    # GLOBAL FUNCS
    global ensureSceneLinked

    def execute(self, context):
        scenes = (sha.getScene(sha.levelSceneName), \
            sha.getScene(sha.assetSceneName), \
            sha.getScene(sha.mshNavSceneName), \
            sha.getScene(sha.mshAreaSceneName) )

        assetFolder = context.scene.riotAssetFolder

        self.report(\
            {'INFO'},\
            'Refreshed')

        libPath =\
            bpy.path.abspath(context.scene.riotAssetFolder).replace("\\", "/")
        self.refreshLibrary( context, libPath )

        if context.screen.scene is not scenes[0]:
            context.screen.scene = scenes[0]
        for scene in bpy.data.scenes:
            if scene not in scenes:
                bpy.data.scenes.remove(scene)
                continue
            scene.riotAssetFolder = assetFolder


        return {'FINISHED'}

    # Updates the files from the library folder that have different timestamps
    def refreshLibrary(self, context, libPath):
        obsUpdated = 0
        obsOld = 0
        obsNew = 0
        obsLinked = 0
        obsTotal = 0
        # THIS VARIABLE IS NOT USED YET:
        obsRemoved = 0

        obsUpdatedStr = []
        obsOldStr = []
        obsNewStr = []
        obsLinkedStr = []
        obsTotalStr = []
        # THIS VARIABLE IS NOT USED YET:
        obsRemovedStr = []
        
        prePath = libPath.rpartition("assets")[0]
        feDae = ".dae"

        lvlScn = sha.getScene(sha.levelSceneName)
        assScn = sha.getScene(sha.assetSceneName)

        chckedObs = []

        assetsPaths = self.getFilePaths(context, libPath, "*.lua")
        if not assetsPaths:
            self.report(\
                {'ERROR'},\
        'The object folder in the asset folder at "{}" contains no .lua files'\
                .format(libPath))
            return

        # Update and create models, put their materials in their ID prop.
        for path in assetsPaths:
            mdl, mats = self.getObjectAssetPaths(path)
            
            if not os.path.exists(prePath + mdl + feDae):
                self.report( \
                    {'WARNING'}, \
                    'Could not find file on update: {}' \
                    .format(prePath + mdl + feDae) )
                continue

            timeStamp = int(os.path.getmtime(prePath + mdl + feDae))
            assetName = path.rpartition("/assets/")[2]
            asset = None
            exists = False
            rotate = False

            if assetName in assScn.objects:
                exists = True

            if exists:
                obsTotal += 1
                obsTotalStr.append(assetName)
                obsOld += 1
                obsOldStr.append(assetName)
                asset = bpy.data.objects[assetName]
                self.ensureGroup(asset)
                
                if timeStamp != asset.riotTimestamp:
                    obsUpdated += 1
                    obsUpdatedStr.append(assetName)
                    obsOld -= 1
                    obsOldStr.pop()
                    asset.riotTimestamp = timeStamp
                    junk, rotate = self.createAsset(\
                        context, prePath + mdl + feDae, assetName)
                    if not asset:
                        self.report( \
                            {'ERROR'}, \
                            'Failed to refresh: {}' \
                            .format(path) )
                        continue
                elif assScn not in asset.users_scene:
                    obsLinked += 1
                    obsLinkedStr.append(assetName)
                    assScn.objects.link(asset)
            else:
                obsTotal += 1
                obsNew += 1
                asset, rotate = self.createAsset(\
                    context, prePath + mdl + feDae, assetName)
                if not asset:
                    self.report( \
                        {'ERROR'}, \
                        'Failed to import: {}' \
                        .format(path) )
                    continue
                obsTotalStr.append(assetName)
                obsNewStr.append(assetName)
                asset.riotTimestamp = timeStamp

            if asset is not None:
                asset.riotMaterialNames = ""
                for mat in mats:
                    asset.riotMaterialNames += mat + ","

                    matPath = "{}/{}".format( \
                        lvlScn.riotAssetFolder.rpartition("/assets/")[0], \
                        mat)

                    if not os.path.exists(matPath):
                        self.report(\
                            {'WARNING'},\
                            '{} references missing material: {}'\
                            .format( prePath + mdl, prePath + mat ))
                
                asset.riotMaterialNames = asset.riotMaterialNames.rstrip(",")
                asset.location = (0, 0, 0)
                asset.scale = (1, 1, 1)

            chckedObs.append(asset)

        usedObs = []

        for ob in lvlScn.objects:
            if ob.type == 'EMPTY' \
            and ob.riotType \
            and ob.riotType == 'mdl-static' \
            and ob.dupli_group:
                usedObs.append( ob.dupli_group.objects[0] )

        for ob in assScn.objects:
            if ob in usedObs \
            and ob not in chckedObs:
                for empty in lvlScn.objects:
                    if empty.type == 'EMPTY' \
                    and empty.riotType \
                    and empty.riotType == 'mdl-static' \
                    and empty.dupli_group \
                    and len( empty.dupli_group.objects ) == 1\
                    and empty.dupli_group.objects[0].name == ob.name:
                        self.report(\
                            {'ERROR'},\
                            'No object (.lua) found for asset used by "{}": {}'\
                            .format( empty.name, ob.name ))



        """
        print("\nRefreshed assets")
        print("All: {}\n{}\n"\
            .format(obsTotal, obsTotalStr))
        print("Updated: {}\n{}\n"\
            .format(obsUpdated, obsUpdatedStr))
        print("New: {}\n{}\n"\
            .format(obsNew, obsNewStr))
        print("Old: {}\n{}\n"\
            .format(obsOld, obsOldStr))
        print("Linked: {}\n{}\n"\
            .format(obsLinked, obsLinkedStr))
        print("Removed: {}\n{}\n"\
            .format(obsRemoved, obsRemovedStr))
        """
        
        return

    # Takes a path to an object file and returns a tuple with a path to a model
    # and to its associated materials based on the contents of the object files.
    def getObjectAssetPaths(self, inputPath):
        outputPaths = (str, [])

        f = open(inputPath, 'r', encoding='utf-8')
        entireFile = f.read()
        f.close()

        mdlPath =\
        entireFile.partition('"')[2].partition('"')[0].rpartition(".")[0]
        mtlPath = []

        mtlPathStep = entireFile.partition('"')[2].partition('"')[2]

        while mtlPathStep.find('"') != -1:
            mtlPath.append(mtlPathStep.partition('"')[2].partition('"')[0])
            mtlPathStep = mtlPathStep.partition('"')[2].partition('"')[2]

        outputPaths = (mdlPath, mtlPath)

        return outputPaths

    # Returns the paths to all files in the specified dir with the specified
    # file endings.
    def getFilePaths(self, context, inputDirectory, fileEnding):
        filepaths = []
        for dirpath, dirnames, filenames in os.walk(inputDirectory):
            filtered = fnmatch.filter(filenames, fileEnding)

            for item in filtered:
                fullPaths = os.path.join(dirpath, item)
                filepaths.append(fullPaths.replace("\\", "/"))
        return filepaths

    # Creates an asset, groups it if needed and puts it in the asset scene.
    # If it is supposed to be updated, it instead replaces the
    # mesh data of the object.
    def createAsset(self, context, assetPath, assetName):
        global ensureSceneLinked
        assetScene = sha.getScene(sha.assetSceneName)

        assetObj, rotate = self.importCollada(context, assetPath)
        self.ensureSceneUnlinked(assetObj, context.scene)

        assetIndex = bpy.data.objects.find(assetName)
        if assetIndex == -1:
            ensureSceneLinked(assetObj, assetScene)
        else:
            oldObj = bpy.data.objects[assetIndex]
            oldObj.data = assetObj.data.copy()
            assetObj = oldObj

        assetObj.name = assetName

        status = self.ensureGroup(assetObj)
        if not status:
            return (None, False)

        ensureSceneLinked(assetObj, assetScene, True)
        return (assetObj, rotate)

    # Makes sure the asset is NOT the specified scene.
    def ensureSceneUnlinked(self, inputAsset, inputScene):
        for scene in bpy.data.scenes:
            if scene == inputScene:

                for asset in scene.objects:
                    if asset == inputAsset:
                        inputScene.objects.unlink(inputAsset)
                        return True

                return True
        return False

# Makes sure the asset is in a group by the same name as the last part of its
# name.
    def ensureGroup(self, asset):
        if not asset \
        or not asset.name \
        or asset.name.find("/texture/") != -1:
            return False

        groupName = asset.name.rpartition("/model/")[2]

        for group in asset.users_group:
            if group.name == groupName:
                return True
        
        for group in bpy.data.groups:
            if group.name == groupName:
                group.objects.link(asset)
                return True

        group = bpy.data.groups.new(groupName)
        group.objects.link(asset)
        return True

# Imports a collada file from the set asset folder and returns it.
    def importCollada(self, context, inputPath):
        bpy.ops.wm.collada_import(\
            filepath=inputPath,\
            filter_blender=False,\
            filter_backup=False,\
            filter_image=False,\
            filter_movie=False,\
            filter_python=False,\
            filter_font=False,\
            filter_sound=False,\
            filter_text=False,\
            filter_btx=False,\
            filter_collada=True,\
            filter_folder=True,\
            filemode=8,\
            display_type='FILE_DEFAULTDISPLAY',\
            import_units=False)

        rotate = self.yUp(inputPath)

        allOb = context.selected_objects
        outOb = None

        for ob in allOb:
            if not ob:
                pass

            elif ob.type != 'MESH':
                context.scene.objects.unlink( ob )
                self.report(\
                    {'WARNING'},\
                    '{} contains unnecessary object: {}'\
                    .format( inputPath, ob.name ))\

            elif not outOb:
                outOb = ob
                
            elif outOb:
                context.scene.objects.unlink( ob )
                self.report(\
                    {'ERROR'},\
                    '{} contains more than one mesh: {}'\
                    .format( inputPath, ob.name ) )
        
        if not outOb:
            self.report(\
                {'ERROR'},\
                '{} contains no mesh'\
                .format( inputPath, ob.name ) )

        return (outOb, rotate)

# Checks if the imported collada asset should be rotated or not.
    def yUp(self, filePath):
        f = open(filePath, 'r', encoding='utf-8')
        entireFile = f.read()
        f.close()

        upAxis = entireFile.partition("<up_axis>")[2].partition("</up_axis>")[0]

        if upAxis == "Y_UP":
            return True
        else:
            return False


# # # # # # # # # # # # # # # # # # # #
# ADD SELECTION TO GROUP
class AddSelectionToGroup(bpy.types.Operator):
    """Add selected object to active object's group"""
    bl_idname = "riot.add_selection_to_group"
    bl_label = "Add To Group"

    def execute(self, context):
        return {'FINISHED'}


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# ENTITY OPERATORS

# EDIT MSH AREA OPERATOR
# Puts the area mesh into a mode where it is ready to be edited
class MshEdit(bpy.types.Operator):
    '''Toggles edit mode on the mesh'''
    bl_idname = "riot.msh_edit"
    bl_label = "Edit Mesh"

    # GLOBAL FUNCTIONS
    global createGroupedMeshObject, editDupliGroupObject,\
    doneEditDupliGroupObject, setUpSceneColors, ensureSceneLinked

    @classmethod
    def poll(self, context):
        if len(context.selected_objects) > 0:
            return True
        return False

    def execute(self, context):
        lvlScn = sha.getScene(sha.levelSceneName)
        empty = None

        areScn = sha.getScene(sha.mshAreaSceneName)
        navScn = sha.getScene(sha.mshAreaSceneName)
        scenes = { \
            'msh-area':areScn, \
            'msh-nav':navScn \
        }

        if lvlScn.riotMshEdit:
            empty = bpy.data.objects[lvlScn.riotMshEdit]
            doneEditDupliGroupObject(context, empty)
            lvlScn.riotMshEdit = ''

        else:
            empty = context.active_object

            createGroupedMeshObject(context, empty, scenes[empty.riotType], empty.riotType)
            editDupliGroupObject(context, empty)
            lvlScn.riotMshEdit = empty.name

        if empty:
            ensureSceneLinked( empty.dupli_group.objects[0], \
                bpy.data.scenes[sha.getScene(empty.riotType).name], True )
        
        setUpSceneColors()

        return{'FINISHED'}

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# PROPERTY PANELS

# RIOT LIGHT PANEL
class RiotLight(bpy.types.Panel):
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_label = "Kravall Global Light"

    def draw(self, context):
        layout = self.layout

        sIntense = context.scene.riotSunIntensity
        sCol = context.scene.riotSunColor
        sDir = context.scene.riotSunDirection

        col = layout.column(align = False)
        col.label("Ambient:")
        col.prop(context.scene, "riotAmbientIntensity", text = "Intensity")
        col.prop(context.scene, "riotAmbientColor", text = "")

        col = layout.column(align = False)
        col.label("Sun:")
        col.prop(context.scene, "riotSunIntensity", text = "Intensity")
        col.prop(context.scene, "riotSunColor", text = "")
        col.prop(context.scene, "riotSunDirection", text = "")
        row = col.row()
        row.label( "X: {0:.2f}".format( sDir.x ) )
        row.label( "Y: {0:.2f}".format( sDir.y ) )
        row.label( "Z: {0:.2f}".format( sDir.z ) )

# ENTITY PROPERTY PANEL
class RiotEntProperty(bpy.types.Panel):
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_label = "Kravall Entity"
    
    @classmethod
    def poll(self, context):
        if context.active_object is not None:
            obType = context.active_object.type
            return obType == 'EMPTY'\
            or obType == 'LAMP'\
            or obType == 'CAMERA'
        return False
    
    def draw(self, context):
        global icon
        ob = context.active_object
        layout = self.layout

        col = layout.column()
        col.prop(ob, "riotName", "Name")

        if ob and ob.type == 'EMPTY':
            row = col.row()
            row.label("Type:")
            row.prop_menu_enum(ob, "riotType", ob.riotType)

            if context.active_object.riotType == 'msh-nav':
                col = layout.column()
                col.prop_search(\
                    ob, 'dupli_group', bpy.data, 'groups', text = "Object")
            elif context.active_object.riotType == 'msh-area':
                col = layout.column()
                col.prop_search(\
                    ob, 'dupli_group', bpy.data, 'groups', text = "Object")
            elif context.active_object.riotType == 'mdl-static':
                col = layout.column()
                col.prop_search(\
                    ob, 'dupli_group', bpy.data, 'groups', text = "Object")
                if ob.dupli_group and len(ob.dupli_group.objects) > 0:
                    mshOb = ob.dupli_group.objects[0]
                    matList = mshOb.riotMaterialNames.split(",")
                    
                    if len(matList) > 1:
                        col = layout.column(align = True)
                        col.prop(ob, "riotSelectedMaterial", "Material")
                        for mat in range(len(matList)):
                            col.label("{} = {}".format(mat, bpy.path.display_name_from_filepath(matList[mat])))
                    col.separator()
            elif context.active_object.riotType == 'decal':
                row = col.row( align = True )
                row.prop( ob, "riotDecalPath", "Material" )
                row.operator("riot.decal_browser_single", "", icon = icon['folder'])


        elif ob.type == 'LAMP':
            if ob.data.type == 'POINT':
                col.label("Point Light:")
                col.prop(ob.data, "use_sphere", text = "Show Sphere")
                row = col.row()
                row.label("Color:")
                row.prop(ob.data, "color", text = "")
                col.prop(ob.data, "energy")
                col.prop(ob.data, "distance")

            if ob.data.type == 'SPOT':
                col.label("Spot Light:")
                col.prop(ob.data, "show_cone", text = "Show Cone")
                row = col.row()
                row.label("Color:")
                row.prop(ob.data, "color", text = "")
                col.prop(ob.data, "energy")
                col.prop(ob.data, "distance")
                col.prop(ob.data, "spot_size")
                col.prop(ob.data, "spot_blend")

        elif ob.type == 'CAMERA':
            col.label("Camera:")
            col.prop(ob.data, "angle")

        if ob.type != 'CAMERA':
            col.label("Callbacks:")
            col.prop(ob, "riotInitFunc")
            col.prop(ob, "riotUpdateFunc")
            col.prop(ob, "riotTickFunc")
            col.prop(ob, "riotDestroyFunc")


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# TOOL PANELS

# RIOT SETTINGS PANEL
class RiotSettings(bpy.types.Panel):
    bl_space_type = "VIEW_3D"
    bl_region_type = "TOOLS"
    bl_label = "Kravall Settings"

    def draw(self, context):
        global icon

        layout = self.layout

        col = layout.column(align = True)
        col.label("Assets Folder:")
        col.prop(context.scene, "riotAssetFolder", "")
        row = col.row(align = True)
        col2 = row.column(align = True)
        col2.enabled = ( context.scene.riotAssetFolder != '' )
        col2.operator( "riot.refresh_assets", icon='FILE_REFRESH' )
        col3 = row.column(align = True)
        col3.operator( "riot.purge_assets", icon='X' )

        col = layout.column(align = True)
        col.label("Export:")
        col.prop(context.scene, "riotExportPath", "")
        row = col.row(align = True)
        row.prop(context.scene, "exportNav", icon = 'MESH_GRID')
        row.prop(context.scene, "exportArea", icon = 'MESH_PLANE')
        row.prop(context.scene, "exportMdlStatic", icon = 'MESH_CUBE')
        row = col.row(align = True)
        row.prop(context.scene, "exportEmpty", icon = 'OUTLINER_OB_EMPTY')
        row.prop(context.scene, "exportCamera", icon = 'CAMERA_DATA')
        row.prop(context.scene, "exportLight", icon = 'LAMP')
        row = col.row(align = True)
        row.prop(context.scene, "exportDecal", icon = icon['decal'])
        row.label()
        row.label()


# RIOT TOOL PANEL
class RiotTool(bpy.types.Panel):
    bl_space_type = "VIEW_3D"
    bl_region_type = "TOOLS"
    bl_label = "Kravall Tools"

    def draw(self, context):
        global icon
        ob = context.active_object

        layout = self.layout

        col = layout.column(align = False)
        col.enabled = ( context.scene.riotExportPath != '' )
        col.operator("riot.quick_export", icon = icon['export'])

        col = layout.column( align = True )
        col.label("Create (Ctrl Shift A):")

        row = col.row( align = False )
        col = row.column( align = True )
        col.operator("riot.decal_browser", icon = icon['folder'])
        col.operator("riot.add_decal", "Decal", icon = icon['decal'])
        
        col = row.column( align = True )
        col.operator("riot.asset_browser", "Models", icon = icon['folder'])
        col.operator("riot.add_mdl_static", "Model", icon = icon['mdl-static'])
        
        col = layout.column( align = True )
        row = col.row( align = True )
        row.operator("riot.add_light_spot", "Spot", icon = icon['light-spot'])
        row.operator("riot.add_light_point", "Point", icon = icon['light-point'])

        col = layout.column( align = True )
        row = col.row( align = True )
        row.operator("riot.add_camera", "Cam", icon = icon['camera'])
        row.operator("riot.add_area", "Area", icon = icon['msh-area'])
        row = col.row( align = True )
        row.operator("riot.add_empty", "Empty", icon = icon['empty'])
        row.operator("riot.add_nav", "Nav", icon = icon['msh-nav'])
        
        
        
        col = layout.column( align = False )
        col.label("Select:")
        col = layout.column( align = True )
        col.enabled = ( ob != None )
        col.operator("riot.select_by_asset", "By Asset")

        
        col.label("Edit:")

        btnTxt = "Edit Mesh"
        col = layout.column( align = True )
        col.enabled = False
        if ob:
            col.enabled = ( ob.riotType == 'msh-area' \
                or ob.riotType == 'msh-nav' \
                or context.mode == 'EDIT_MESH' )
            
            if context.scene.riotMshEdit:
                btnTxt = "Done Editing"
            elif not context.active_object.dupli_group:
                btnTxt = "Create Mesh"

        col.operator("riot.msh_edit", btnTxt)

class purgeAssets(bpy.types.Operator):
    bl_idname = "riot.purge_assets"
    bl_label = "Purge"
    bl_description = "Remove all assets from the library in the .blend file"

    def execute(self, context):
        assScn = sha.getScene(sha.assetSceneName)
        for ob in assScn.objects:
            ob.data.user_clear()
            for scene in ob.users_scene:
                scene.objects.unlink(ob)
            for group in ob.users_group:
                group.objects.unlink(ob)
            ob.user_clear()

        return{'FINISHED'}

class quickExport(bpy.types.Operator):
    bl_idname = "riot.quick_export"
    bl_label = "Quick Export"
    bl_description = "One-button export to the directory above"

    def execute(self, context):
        scn = context.scene

        if not scn:
            return{'FINISHED'}

        if not scn.riotExportPath or scn.riotExportPath == "":
            self.report(\
                {'ERROR'},\
                'No export path has been set')
            return{'FINISHED'}

        if scn.riotExportPath.find(".lua") == -1:
            scn.riotExportPath += ".lua"

        scn.riotExportPath = scn.riotExportPath.replace("\\", "/")

        if scn.riotExportPath.find("/scenarios/") == -1:
            self.report(\
                {'ERROR'},\
                'Export path needs to be under the "scenarios" dir')
            return{'FINISHED'}

        path = bpy.path.abspath(scn.riotExportPath)

        self.report(\
                {'INFO'},\
                'Exported')

        bpy.ops.export.riot_level(\
            filepath = path,\
            export_nav = scn.exportNav,\
            export_area = scn.exportArea,\
            export_mdl_static = scn.exportMdlStatic,\
            export_empty = scn.exportEmpty,\
            export_camera = scn.exportCamera,\
            export_light = scn.exportLight,\
            export_decal = scn.exportDecal\
            )

        return{'FINISHED'}

class createPointLight(bpy.types.Operator):
    bl_idname = "riot.add_light_point"
    bl_label = "Point Light"
    bl_description = "Place a new point light on your 3D cursor"

    def execute(self, context):
        lvlScn = sha.getScene( sha.levelSceneName )

        pl = bpy.data.lamps.new( "PointLight", 'POINT' )
        pl.use_sphere = True
        pl.distance = 5

        obPl = bpy.data.objects.new( "Point ", pl)
        obPl.location = lvlScn.cursor_location

        lvlScn.objects.link( obPl )

        return{'FINISHED'}

class createSpotLight(bpy.types.Operator):
    bl_idname = "riot.add_light_spot"
    bl_label = "Spot Light"
    bl_description = "Place a new spot light on your 3D cursor"

    def execute(self, context):
        lvlScn = sha.getScene( sha.levelSceneName )

        sl = bpy.data.lamps.new( "SpotLight", 'SPOT' )
        sl.distance = 5
        sl.spot_blend = .5

        obSl = bpy.data.objects.new( "Spot ", sl)
        obSl.location = lvlScn.cursor_location

        lvlScn.objects.link( obSl )

        return{'FINISHED'}

class createCamera(bpy.types.Operator):
    bl_idname = "riot.add_camera"
    bl_label = "Camera"
    bl_description = "Place a new camera on your 3D cursor"

    def execute(self, context):
        lvlScn = sha.getScene( sha.levelSceneName )

        cam = bpy.data.cameras.new( "Camera" )
        cam.lens_unit = 'FOV'

        camOb = bpy.data.objects.new( "Camera", cam)
        camOb.location = lvlScn.cursor_location

        lvlScn.objects.link( camOb )

        return{'FINISHED'}

class createMdlStatic(bpy.types.Operator):
    """Places a new mdl-static entity on the 3D cursor"""
    bl_idname = "riot.add_mdl_static"
    bl_label = "Model"

    def execute(self, context):
        bpy.ops.riot.add_entity()
        return{'FINISHED'}

class AddEntity(bpy.types.Operator):
    '''Places an entity on the 3D cursor'''
    bl_idname = "riot.add_entity"
    bl_label = "Add Entity"

    def execute(self, context):
        context.screen.scene = sha.getScene(sha.levelSceneName)

        ob = bpy.data.objects.new("entity", None)
        context.scene.objects.link(ob)
        ob.dupli_type = 'GROUP'

        ob.location = context.scene.cursor_location
        context.scene.objects.active = ob
        self.deselectAll(context.scene)
        ob.select = True

        return {'FINISHED'}

    def deselectAll(self, scene):
        for ob in scene.objects:
            ob.select = False

class AddEmpty(bpy.types.Operator):
    '''Places a new empty entity on the 3D cursor'''
    bl_idname = "riot.add_empty"
    bl_label = "Empty"

    def execute(self, context):
        bpy.ops.riot.add_entity()

        ob = context.active_object
        ob.riotType = 'empty'

        return{'FINISHED'}

class AddMshNav(bpy.types.Operator):
    '''Places a new msh-nav entity on the 3D cursor'''
    bl_idname = "riot.add_nav"
    bl_label = "Nav"

    def execute(self, context):
        bpy.ops.riot.add_entity()

        ob = context.active_object
        ob.riotType = 'msh-nav'

        return{'FINISHED'}

class AddMshArea(bpy.types.Operator):
    '''Places a new msh-area entity on the 3D cursor'''
    bl_idname = "riot.add_area"
    bl_label = "Area"

    def execute(self, context):
        bpy.ops.riot.add_entity()

        ob = context.active_object
        ob.riotType = 'msh-area'

        return{'FINISHED'}

class AddDecal(bpy.types.Operator):
    '''Places a new decal on the 3D cursor'''
    bl_idname = "riot.add_decal"
    bl_label = "Decal"

    groupName = "decal"

    def execute(self, context):
        bpy.ops.riot.add_entity()

        ob = context.active_object
        ob.riotType = 'decal'
        ob.dupli_group = self.getDecalGroup( context )
        ob.empty_draw_type = 'ARROWS'
        ob.empty_draw_size = 0.5

        return{'FINISHED'}

    def getDecalGroup(self, context):
        global ensureSceneLinked
        othScn = sha.getScene(sha.otherSceneName)
        decalGroup = None

        for group in bpy.data.groups:
            if group.name == self.groupName and group.objects:
                decalGroup = group

        if decalGroup == None:
            bpy.ops.mesh.primitive_cube_add()
            ob = context.active_object
            ob.location = ( 0, 0, 0 )
            ob.draw_type = 'WIRE'
            ob.scale = ( 0.5, 0.5, 0.5 )
            decalGroup = \
                self.ensureGroup( ob, self.groupName )

        ensureSceneLinked( decalGroup.objects[0], othScn, True )
        
        return decalGroup

    def ensureGroup(self, ob, groupName):
        if groupName not in bpy.data.groups:
            bpy.data.groups.new(groupName)

        outGroup = bpy.data.groups[groupName]

        outGroup.objects.link(ob)

        return outGroup

class assetBrowser(bpy.types.Operator):
    '''Asset browser operator'''
    bl_idname = "riot.asset_browser"
    bl_description = "Browse for riot asset files (.lua)"
    bl_label = "Models"

    #filepath = bpy.props.StringProperty(subtype="FILE_PATH")
    #filename = bpy.props.StringProperty(subtype="FILE_NAME")
    directory = bpy.props.StringProperty(subtype="DIR_PATH")
    files = bpy.props.CollectionProperty(type=bpy.types.OperatorFileListElement)

    filename_ext = ".lua"
    filter_glob = bpy.props.StringProperty(
        default="*.lua",
        options={'HIDDEN'}
    )

    def execute(self, context):
        obList = []

        lvlScn = sha.getScene(sha.levelSceneName)
        assScn = sha.getScene(sha.assetSceneName)

        self.directory = self.directory.replace("\\", "/")

        for item in self.files:
            path = ( self.directory + item.name ).rpartition("/assets/")[2]

            if path in assScn.objects:
                bpy.ops.riot.add_entity()
                ob = context.active_object
                ob.dupli_group = bpy.data.groups[path]
                obList.append(ob)
            else:
                self.report(\
                    {'ERROR'},\
                    'Unable to find asset in library: {}'\
                    .format( path ))

        for ob in obList:
            ob.select = True

        if len(obList) > 0:
            self.report(\
                {'INFO'},\
                'Added {} mdl_static entities'\
                .format( len(obList) ))

        return{'FINISHED'}

    def invoke(self, context, event):
        bpy.context.window_manager.fileselect_add(self)
        return{'RUNNING_MODAL'}

class decalBrowser(bpy.types.Operator):
    '''Decal browser operator'''
    bl_idname = "riot.decal_browser"
    bl_description = "Browse for decal material files (.material)"
    bl_label = "Decals"

    #filepath = bpy.props.StringProperty(subtype="FILE_PATH")
    #filename = bpy.props.StringProperty(subtype="FILE_NAME")
    directory = bpy.props.StringProperty(subtype="DIR_PATH")
    files = bpy.props.CollectionProperty(type=bpy.types.OperatorFileListElement)

    filename_ext = ".material"
    filter_glob = bpy.props.StringProperty(
        default="*.material",
        options={'HIDDEN'}
    )

    def execute(self, context):
        decalList = []
        lvlScn = sha.getScene(sha.levelSceneName)
        dirFrontSlash = self.directory.replace('\\', '/')

        for item in self.files:
            path = ( dirFrontSlash + item.name ).rpartition("/assets/")[2]

            bpy.ops.riot.add_decal()
            ob = context.active_object
            ob.riotDecalPath = path
            decalList.append( ob )

        for ob in decalList:
            ob.select = True

        if len(decalList) > 0:
            self.report(\
                {'INFO'},\
                'Added {} decal entities'\
                .format( len(decalList) ))

        return{'FINISHED'}

    def invoke(self, context, event):
        bpy.context.window_manager.fileselect_add(self)
        return{'RUNNING_MODAL'}

class decalBrowserSingle(bpy.types.Operator):
    '''Decal browser operator'''
    bl_idname = "riot.decal_browser_single"
    bl_description = "Browse for a decal material file (.material)"
    bl_label = "Select Material"

    filepath = bpy.props.StringProperty(subtype="FILE_PATH")
    #filename = bpy.props.StringProperty(subtype="FILE_NAME")
    #directory = bpy.props.StringProperty(subtype="DIR_PATH")
    #files = bpy.props.CollectionProperty(type=bpy.types.OperatorFileListElement)

    filename_ext = ".material"
    filter_glob = bpy.props.StringProperty(
        default="*.material",
        options={'HIDDEN'}
    )

    def execute(self, context):
        pathFrontSlash = self.filepath.replace('\\', '/')
        ob = context.active_object
        ob.riotDecalPath = pathFrontSlash.rpartition("/assets/")[2]

        return{'FINISHED'}

    def invoke(self, context, event):
        assetFolder = context.scene.riotAssetFolder
        ob = context.active_object
        self.filepath = assetFolder + ob.riotDecalPath

        bpy.context.window_manager.fileselect_add(self)
        return{'RUNNING_MODAL'}

class selectByAsset(bpy.types.Operator):
    '''Select all entities using the same assets as the selected entity'''
    bl_idname = "riot.select_by_asset"
    bl_description = "Select all entities using the " + \
        "same assets as the selected entities"
    bl_label = "Select By Asset"

    def execute(self, context):
        lvlScn = sha.getScene(sha.levelSceneName)
        selObs = context.selected_objects

        for ob in selObs:
            if ob.type == 'EMPTY':
                asset = ob.dupli_group

            for ob2 in lvlScn.objects:
                if ob2.type == 'EMPTY' \
                and ob2.dupli_group == asset:
                    ob2.select = True

        return{'FINISHED'}

class riotCreateMenu(bpy.types.Menu):
    bl_label = "Create"
    bl_idname = "riot.create_menu"

    def draw(self, context):
        global icon

        layout = self.layout
        layout.operator("riot.add_mdl_static", icon = icon['mdl-static'])
        layout.operator("riot.add_decal", icon = icon['decal'])
        layout.operator("riot.add_light_point", icon = icon['light-point'])
        layout.operator("riot.add_light_spot", icon = icon['light-spot'])
        layout.separator()
        layout.operator("riot.add_area", icon = icon['msh-area'])
        layout.operator("riot.add_nav", icon = icon['msh-nav'])
        layout.operator("riot.add_camera", icon = icon['camera'])
        layout.operator("riot.add_empty", icon = icon['empty'])

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# MISC

addon_keymaps = []

def register():
    bpy.utils.register_module(__name__)


    # Handle the keymap.
    kc = bpy.context.window_manager.keyconfigs.addon
    if kc:
        km = kc.keymaps.new(name="Object Mode", space_type='EMPTY')
        kmi = km.keymap_items.new('riot.quick_export', 'E', 'PRESS', ctrl=True)
        addon_keymaps.append(km)

        km = kc.keymaps.new(name="Object Mode", space_type='EMPTY')
        kmi = km.keymap_items.new("wm.call_menu", 'A', 'PRESS', ctrl=True, shift=True, head = True)
        kmi.properties.name = 'riot.create_menu'
        addon_keymaps.append(km)

def unregister():
    bpy.utils.unregister_module(__name__)

    # Handle the keymap.
    wm = bpy.context.window_manager
    for km in addon_keymaps:
        wm.keyconfigs.addon.keymaps.remove(km)

    addon_keymaps.clear()

if __name__ == "__main__":
    register()
